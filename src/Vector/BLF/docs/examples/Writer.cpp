#include <iostream>
#include <fstream>

#include "Vector/BLF.h"

static void showCanMessage(Vector::BLF::CanMessage * canMessage)
{
    std::cout << "CanMessage:"
              << " ch=" << canMessage->channel
              << " flags=" << (unsigned short) canMessage->flags
              << " dlc=" << (unsigned short) canMessage->dlc
              << " id=0x" << std::hex << canMessage->id
              << " data=";
    for (int i = 0; (i < canMessage->dlc) && (i < 8); ++i) {
        std::cout << std::hex;
        if (canMessage->data[i] <= 0xf) {
            std::cout << '0';
        }
        std::cout << ((unsigned short) canMessage->data[i]);
        std::cout << " ";
    }
    std::cout << std::endl;
}

int main(int argc, char * argv[])
{
    if (argc != 2) {
        std::cout << "Writer <filename.blf>" << std::endl;
        return -1;
    }

    Vector::BLF::File file;
    file.open(argv[1], Vector::BLF::File::OpenMode::Write);

    Vector::BLF::CanMessage message;
    message.signature = 0x4A424F4C;
    message.headerSize = 0x20;
    message.headerVersion = 0x01;
    message.objectSize = 56;
    message.objectVersion = 1;
    message.reserved = 0;
    message.channel = 1;
    message.flags = 0;
    message.objectTimeStamp = 1000;
    message.objectFlags = Vector::BLF::ObjectHeader::ObjectFlags::TimeOneNans;
    message.dlc = 8;
    message.id = 0x100;
    message.data[0] = 0x0;
    message.data[1] = 0x0;
    message.data[2] = 0x0;
    message.data[3] = 0x0;
    message.data[4] = 0x0;
    message.data[5] = 0x0;
    message.data[6] = 0x0;
    message.data[7] = 0x0;
    file.write(reinterpret_cast<Vector::BLF::ObjectHeaderBase *>(&message));
    showCanMessage(&message);

    message.channel = 2;
    message.objectTimeStamp = 10000;
    message.dlc = 5;
    message.id = 0x7FF;
    message.data[0] = 0x1;
    message.data[1] = 0x2;
    message.data[2] = 0x3;
    message.data[3] = 0x4;
    message.data[4] = 0x5;
    message.data[5] = 0x0;
    message.data[6] = 0x0;
    message.data[7] = 0x0;
    file.write(reinterpret_cast<Vector::BLF::ObjectHeaderBase *>(&message));
    showCanMessage(&message);

    file.close();

    return 0;
}
